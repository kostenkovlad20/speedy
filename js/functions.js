$(document).ready(function() {

    $('.menu-icon').on('click', function() {
        $('.navigation').toggleClass('navigation-opacity');
        $(this).toggleClass('menu-icon-active');
        $('.wrapper').toggleClass('is-menu-open');
    });

    $('.added-plus').on('click', function() {
        $('.plus-item-active').not($(this).closest('.plus').find('.plus-item')).removeClass('plus-item-active');
        $(this).closest('.plus').find('.plus-item').toggleClass('plus-item-active');
    });

    $('.minus').on('click', function() {
        $(this).closest('.plus').find('.plus-item-active').removeClass('plus-item-active');
    });
}); 